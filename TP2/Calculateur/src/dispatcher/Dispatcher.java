package dispatcher;

import java.io.*;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import shared.*;


// Classe qui représente les informations principales du serveur.
class ServerInfo {
    public CalcServerInterface stub;                         // Son interface pour communiquer via java-rmi
    public int                 numGuaranteedOps;             // Son nombre d'opération max avant la possibilité de refus de calculation
    public FutureTask<Integer> task;                         // Utilisé pour partir plusieurs exécution des serveurs de calculation en parallèle
    public ArrayList<String>   taskData = new ArrayList<>(); // Opérations à exécuter
}

// Classe qui représente la fonction à exécuter en parallèle.
class TaskFn implements Callable<Integer> {
    private CalcServerInterface stub;    // Interface du serveur de calculation pour communiquer via java-rmi.
    private String            username;  // Username du client.
    private String            password;  // Password du client.
    private ArrayList<String> opsBlock;  // Les opérations à exécuter.

    public TaskFn(CalcServerInterface stub, String username, String password, ArrayList<String> opsBlock) {
        this.stub = stub;
        this.username = username;
        this.password = password;
        this.opsBlock = opsBlock;
    }

    public Integer call ( ) {
        try {
            return stub.execute(this.username, this.password, this.opsBlock);
        }
        catch ( Exception e ) {
            System.out.println(e.toString());
            return ErrorCode.SERVER_ERROR.getNumVal();
        }
    }
}

// Classe principale qui s'occupe de diviser les tâches au serveurs de calculation disponnibles.
public class Dispatcher {
    private HashMap<String, ServerInfo> computationServerStubs = null; // Dictionnaire de tout les serveurs disponnibles (hostname,serverInfo)
    private NameServerInterface         nameServerStub = null;         // Interface du serveur du service de nom
    private String                      username = "";                 // username de l'utilisateur courant
    private String                      password = "";                 // password de l'utilisateur courant
    private boolean                     isSecure;                      // indique si la calculation se fait en mode unsecure ou secure
                                                                       // En mode sécure, on suppose que les résultat des serveurs sont bon
                                                                       // En mode insécure, on doit valider la réponse avec au mois deux serveurs. (plus lent)
    private int                         minNumGuaranteedOps;           // Utilisé dans le monde insécure afin de déterminer le nombre la grandeur de la division des opérations.

    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println("Not Enough Arguments Exception.");
            return;
        }

        Dispatcher client = new Dispatcher();

        client.run(args);
    }



    public Dispatcher() {

        super();
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        this.computationServerStubs = new HashMap<>();
    }

    private void loadServerStubs() {
        // Obtient la liste de tout les serveurs de calcualtions disponnibles et
        // les insère dans le dictionnaire correspondant.
        ArrayList<String> availableServers = null;
        try {
            availableServers = this.nameServerStub.getAvailableServers();
        }
        catch (RemoteException e) {
            System.out.println("Error RemoteException : " + e.getMessage() + "\n");
        }

        if ( availableServers == null )
            return;

        this.minNumGuaranteedOps = Integer.MAX_VALUE;
        for ( String hostname : availableServers ) {
            try {
                Registry registry = LocateRegistry.getRegistry(hostname, 5000);
                CalcServerInterface stub = (CalcServerInterface)registry.lookup("calc_server");
                ServerInfo info = new ServerInfo();
                info.stub = stub;
                info.numGuaranteedOps = stub.getMaxOp();
                this.computationServerStubs.put(hostname, info);
                if (info.numGuaranteedOps < this.minNumGuaranteedOps)
                    this.minNumGuaranteedOps = info.numGuaranteedOps;
            }
            catch (Exception e) {
                System.out.println("Calculation server '" + hostname + "' cannot be reached.");
            }
        }
    }

    private void loadNameServerStub() {
        // Tente d'avoir un connection avec le serveur de service de nom
        try {
            File file = new File ("config/name_server.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String hostname = reader.readLine();
            Registry registry = LocateRegistry.getRegistry(hostname,5000);
            this.nameServerStub = (NameServerInterface)registry.lookup("name_server");
        } catch (NotBoundException e) {
            System.out.println("Error: The name '" + e.getMessage() + "' Is not defined in the registry.\n");
        } catch (AccessException e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        } catch (RemoteException e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        }
    }

    private void run(String[] args) {
        this.username = args[1];
        this.password = args[2];
        this.isSecure = Boolean.parseBoolean((args[3]));

        this.loadNameServerStub();
        this.loadServerStubs();
        if ( this.computationServerStubs == null )
            return;

        // Lit tout le fichier d'opérations
        ArrayList<String> ops = extractOperationsFromFile(args[0]);

        System.out.println("Begining computation of " + ops.size() + " operations");

        // Début du dispatching et de la calculation du fichier.
        long startTime = System.nanoTime();

        int finalResult;
        // En Mode "sécure" tout les serveurs sont considérés comme exact
        if (isSecure) {
            finalResult = dispatchComputations(ops);
        }
        // En monde "non-sécure", on doit avoir la même réponse pour un bloc d'opérations sur deux serveurs au minimum.
        else {
            finalResult = dispatchUnsecuredComputations(ops);
        }

        long endTime = System.nanoTime();
        // Fin de la calculation.
        if ( finalResult >= 0 ) {
            System.out.println("The final result is " + finalResult);

            System.out.println("The computations took " + (int)((endTime - startTime) / 1.0e6) + " ms.");
        }
        else {
            System.out.println("Error in computation dispatching : " + ErrorCode.getNameFromValue(finalResult) + " : " + finalResult);
        }
    }

    private int dispatchComputations(ArrayList<String> ops){
        // Utilisé afin de paralléliser le travail de computation
        ExecutorService threadPool = Executors.newFixedThreadPool(this.computationServerStubs.size());

        try {
            // File d'opérations a exécuter
            Queue<String> opsQ = new LinkedList<>(ops);
            int finalResult = 0;

            while ( true ) {
                for ( HashMap.Entry<String, ServerInfo> serverEntry : this.computationServerStubs.entrySet() ) {
                    ArrayList<String> opsBlock = serverEntry.getValue().taskData;
                    opsBlock.clear();

                    // On popule la liste des opérations à raison de deux fois la capacité du serveur
                    // De cette façon on maximise le traitement des opérations (1/5 de refus)
                    for (int i = 0; i < serverEntry.getValue().numGuaranteedOps * 2 && !opsQ.isEmpty(); i++)
                        opsBlock.add(opsQ.poll());
                    if (opsBlock.isEmpty())
                        break;

                    // On prépare la tâche à exécuter
                    TaskFn fn = new TaskFn(serverEntry.getValue().stub, this.username, this.password, opsBlock);
                    serverEntry.getValue().task = new FutureTask<>(fn);

                    // On démarre la calcualtion.
                    threadPool.execute(serverEntry.getValue().task);
                }

                // On obtient les résultats ici.
                ArrayList<String> toBeRemoved = new ArrayList<>();
                for ( HashMap.Entry<String, ServerInfo> s : this.computationServerStubs.entrySet() ) {
                    ArrayList<String> opsBlock = s.getValue().taskData;
                    if ( opsBlock.isEmpty() )
                        break;
                    int result = s.getValue().task.get();
                    if ( result < 0 ) {
                        // Si le serveur à refusé de calculer on remet sa charge de travail dans la liste.
                        if ( result == ErrorCode.TOO_MANY_OPERATIONS.getNumVal() ) {
                            opsQ.addAll(opsBlock);
                        }
                        // Si on perd contact avec le serveur, on le retire de la liste de serveur disponnibles.
                        else if ( result == ErrorCode.SERVER_ERROR.getNumVal() ) {
                            opsQ.addAll(opsBlock);
                            toBeRemoved.add(s.getKey());
                            System.out.println("CalcServer " + s.getKey() + " was killed by his brother Cain.");
                        }
                        // Une erreur fatale c'est produite.
                        else {
                            finalResult = result;
                            break;
                        }
                    }
                    else {
                        finalResult += result;
                        finalResult %= 4000;
                    }
                }

                for ( String s : toBeRemoved )
                    this.computationServerStubs.remove(s);

                if ( this.computationServerStubs.isEmpty() )
                    return ErrorCode.NO_MORE_SERVERS.getNumVal();
                else if ( opsQ.isEmpty() || finalResult < 0 )
                    return finalResult;
            }
        }
        catch (Exception e){
            System.out.println(e);
            return ErrorCode.IM_A_TEAPOT.getNumVal();
        }
        finally {
            threadPool.shutdown();
        }

    }

    // Exécution en mode non-sécurisé des opérations.
    private int dispatchUnsecuredComputations ( ArrayList<String> ops ) {
        ExecutorService threadPool = Executors.newFixedThreadPool(this.computationServerStubs.size());

        try {
            Queue<String> opsQ = new LinkedList<>(ops);
            int finalResult = 0;

            boolean done = false;
            while ( !done ) {
                ArrayList<String> opsBlock = new ArrayList<>();
                // On divise les opérations selon le minimum d'opérations accepté avant possibilité de refus
                // parmis tout les serveurs de calculation.
                for (int i = 0; i < this.minNumGuaranteedOps && !opsQ.isEmpty(); i++)
                    opsBlock.add(opsQ.poll());

                // Chacun des serveurs fera la même séquence d'opération afin de comparer les réponses obtenues
                for ( HashMap.Entry<String, ServerInfo> s : this.computationServerStubs.entrySet() ) {
                    s.getValue().taskData = opsBlock;
                    if (opsBlock.isEmpty()) {
                        break;
                    }

                    TaskFn fn = new TaskFn(s.getValue().stub, this.username, this.password, opsBlock);
                    s.getValue().task = new FutureTask<>(fn);
                    threadPool.execute(s.getValue().task);
                }

                ArrayList<String> toBeRemoved = new ArrayList<>();
                HashMap<Integer, Integer> results = new HashMap<>();
                for ( HashMap.Entry<String, ServerInfo> s : this.computationServerStubs.entrySet() ) {
                    if ( opsBlock.isEmpty() )
                        break;
                    int serverResult = s.getValue().task.get();
                    if ( serverResult == ErrorCode.SERVER_ERROR.getNumVal() ) {
                        toBeRemoved.add(s.getKey());
                        System.out.println("CalcServer '" + s.getKey() + "' was killed by his brother Cain.");
                        continue;
                    }
                    Integer resultsElem = results.get(serverResult);
                    if ( resultsElem == null )
                        results.put(serverResult, 1);
                    else
                        results.put(serverResult, results.get(serverResult) + 1);
                }

                for ( String s : toBeRemoved )
                    this.computationServerStubs.remove(s);

                if ( this.computationServerStubs.isEmpty() )
                    return ErrorCode.NO_MORE_SERVERS.getNumVal();

                // On vérifie la fiabilité de la réponse trouvée.
                int mostReliableResult = -1;
                int mostReliableResultCount = 1;
                for ( HashMap.Entry<Integer, Integer> r : results.entrySet() ) {
                    if ( r.getValue() > mostReliableResultCount ) {
                        mostReliableResultCount = r.getValue();
                        mostReliableResult = r.getKey();
                    }
                }

                // Si aucun des serveurs n'a la meme réponse, il y à trop de serveurs malicieux.
                if ( mostReliableResult == -1 ) {
                    finalResult = ErrorCode.UNRELIABLE_SERVERS.getNumVal();
                }
                else {
                    // Ici au moins deux serveurs on la même réponse.
                    // Si il y a autant de serveurs qui donne la meme réponse que d'autres:
                    // par exemple: 2 serveurs donnent 42 et deux autres 23, il y à ambiguité,
                    // donc les serveurs ne sont pas fiables. Sinon, on garde la solution à laquelle
                    // le plus de serveurs on répondu la même réponse.
                    boolean hasAmbiguity = false;
                    for ( HashMap.Entry<Integer, Integer> r : results.entrySet() ) {
                        hasAmbiguity = r.getValue() == mostReliableResultCount && r.getKey() != mostReliableResult;
                        if ( hasAmbiguity ) {
                            finalResult = ErrorCode.UNRELIABLE_SERVERS.getNumVal();
                            break;
                        }
                    }
                    if ( !hasAmbiguity ) {
                        finalResult += mostReliableResult;
                        finalResult %= 4000;
                    }
                }
                done = opsQ.isEmpty() || finalResult < 0;
            }
            return finalResult;
        }
        catch (Exception e){
            System.out.println(e);
            return ErrorCode.IM_A_TEAPOT.getNumVal();
        }
        finally {
            threadPool.shutdown();
        }
    }

    private ArrayList<String> extractOperationsFromFile(String filePath){
        File file = new File (filePath);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String readLine = "";
            ArrayList<String> list = new ArrayList<>();
            while ((readLine = reader.readLine()) != null){
                list.add(readLine);
            }
            return list;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}