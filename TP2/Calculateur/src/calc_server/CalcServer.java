package calc_server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.AccessException;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.net.InetAddress;
import shared.*;

public class CalcServer implements CalcServerInterface {


	private double errorRate;
	private Random randgen = new Random(LocalDateTime.now().getNano());
	private int maxOperations;
	private NameServerInterface nameServerStub = null;
	private String hostname;

    CalcServer(int maxOperations, double errorRate){
    	super();
    	this.maxOperations = maxOperations;
		this.errorRate = errorRate;
		try {
			InetAddress addr = InetAddress.getLocalHost();
			this.hostname = addr.getHostName();
		} catch (Exception e){
			System.out.println("Error234234: " + e.getMessage());
		}
    }

	public static void main (String[] args) {
		int maxOp = 3;
		if (args.length >= 1){
			maxOp = Integer.parseInt(args[0]);
			System.out.println("Minimum number of guaranteed operations : " + maxOp);
		}

		double errorRate = 0;
		if (args.length == 2){
			errorRate = Double.parseDouble(args[1]);
			System.out.println("Error rate : " + errorRate * 100.0 + "%");
		}

		CalcServer server = new CalcServer(maxOp,errorRate);
		server.run();
	}


	private void run() {
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		try {
			CalcServerInterface stub = (CalcServerInterface) UnicastRemoteObject.exportObject(this, 0);
			Registry registry = LocateRegistry.getRegistry(5000);
			registry.rebind("calc_server", stub);
			this.nameServerStub = this.loadNameServerStub();
			// S'enregistrer en tant que serveur disponnible pour exécuter des opérations auprès du name_server
			this.nameServerStub.registerCalcServer(this.hostname);
			System.out.println("Calculation Server ready.");
		} catch (ConnectException e) {
			System.out.println("Cannot connect to RMI registry. Has rmiregistry been started?\n");
			System.out.println("Error: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
	}

	@Override
    public int execute (String username, String password, ArrayList<String> ops) throws RemoteException {
        try {
	        if ( !this.nameServerStub.isValidUserInfo(username, password) ) {
				return ErrorCode.UNAUTHORIZED.getNumVal();
			}
		}
		catch ( Exception e ) {
			return ErrorCode.NAME_SERVER_ERROR.getNumVal();
		}

        int sum = 0;
        if (!canExecute(ops.size())) {
            return ErrorCode.TOO_MANY_OPERATIONS.getNumVal();
        }
        Operation operation;
        for (String op : ops) {
            operation = Operation.fromString(op);
            switch (operation.cmd) {
                case "pell":
                    sum += pell(operation.arg);
                    sum %= 4000;
                    break;
                case  "prime":
                    sum += prime(operation.arg);
                    sum %= 4000;
                    break;
                default:
                    return ErrorCode.UNKNOWN_COMMAND.getNumVal();
            }
        }
        return sum;
    }

	public int pell(int x){
		int offset = 0;
		if(!isValidAnswer())
		{
			offset = randgen.nextInt(4000);
		}
		return pell(x, offset);
	}

	private int pell(int x, int offset){
		if (x == 0)
			return 0 + offset;
		if (x == 1)
			return 1 + offset;

		return (2 * pell(x - 1, offset) + pell(x - 2,offset)) + offset;

	}

	public int prime(int x){
		int highestPrime = 0;
		for (int i = 1; i <= x; ++i)
		{
			if (isPrime(i) && x % i == 0 && i > highestPrime)
				highestPrime = i;
		}

		if(isValidAnswer()){
			return highestPrime;
		}
		return (highestPrime + randgen.nextInt(4000));
	}

	@Override
	public int getMaxOp() throws RemoteException{
    	return maxOperations;
	}

	private double failRate(int ui){
        if (ui < maxOperations)
            return 0;
        return (ui - maxOperations)/ (maxOperations*5.0);
	}

	private boolean isValidAnswer(){
		return randgen.nextDouble() >= this.errorRate;
	}

	private boolean canExecute(int operationCount){
	    return randgen.nextDouble() >= failRate(operationCount);
    }

    private NameServerInterface loadNameServerStub() {
        NameServerInterface stub = null;
        try {
            File file = new File ("config/name_server.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String hostname = reader.readLine();
            Registry registry = LocateRegistry.getRegistry(hostname,5000);
            stub = (NameServerInterface)registry.lookup("name_server");
        } catch (NotBoundException e) {
            System.out.println("Error: The name '" + e.getMessage() + "' Is not defined in the registry.\n");
        } catch (AccessException e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        } catch (RemoteException e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        }
        return stub;
    }

    private static boolean isPrime(int x) {
		if (x <= 1)
			return false;

		for (int i = 2; i < x; ++i)
		{
			if (x % i == 0)
				return false;
		}
		return true;
	}
}