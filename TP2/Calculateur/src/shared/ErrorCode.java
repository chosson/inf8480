package shared;

import java.util.HashMap;

/**
 * Created by kepana on 18-03-09.
 */
public enum ErrorCode {
    TOO_MANY_OPERATIONS(-1),
    UNKNOWN_COMMAND(-2),
    UNRELIABLE_SERVERS(-3),
    SERVER_ERROR(-4),
    NO_MORE_SERVERS(-5),
    NAME_SERVER_ERROR(-6),
    UNAUTHORIZED(-401),
    IM_A_TEAPOT(-418);

    private int numVal;

    private static HashMap<Integer, String> nameDict = buildDict();

    public static String getNameFromValue ( int val ) {
        return nameDict.get(val);
    }

    ErrorCode(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }

    private static HashMap<Integer, String> buildDict ( ) {
        HashMap<Integer, String> result = new HashMap<>();
        for ( ErrorCode c : ErrorCode.values() )
            result.put(c.getNumVal(), c.name());
        return result;
    }

}