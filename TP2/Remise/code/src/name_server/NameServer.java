package name_server;

/**
 * Created by kepana on 18-03-09.
 */
import shared.*;

import java.io.*;
import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

public class NameServer implements NameServerInterface {
    private HashMap<String, String> users;   // Liste d'utilisateurs
    private ArrayList<String>       servers; // Liste de serveurs disponnibles

    NameServer(){
        super();
        this.servers = new ArrayList<>();
        this.readUsersFile("config/users.txt");
    }

    public static void main (String[] args) {
        NameServer server = new NameServer();
        server.run();
    }


    private void run() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        try {
            NameServerInterface stub = (NameServerInterface) UnicastRemoteObject.exportObject(this, 0);
            Registry registry = LocateRegistry.getRegistry(5000);
            registry.rebind("name_server", stub);
            System.out.println("Name-server Service ready.");

        } catch (ConnectException e) {
            System.err.println("Cannot connect to RMI registry. Has rmiregistry been started?\n");
            System.err.println("Error: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    @Override
    public ArrayList<String> getAvailableServers() throws RemoteException {
        return this.servers;
    }

    @Override
    public boolean isValidUserInfo(String username, String password) throws RemoteException {
        return this.users.containsKey(username) && this.users.get(username).equals(password);
    }

    @Override
    // Sert à enregister un serveur de calculation au serveur de service de nom.
    public boolean registerCalcServer(String hostname) throws RemoteException {
        return this.servers.add(hostname);
    }

    private void readUsersFile(String filename) {
        File file = new File (filename);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            String[] userInfo;
            this.users = new HashMap<>();
            while ((line = reader.readLine()) != null){
                userInfo = line.split(",");
                this.users.put(userInfo[0], userInfo[1]);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}