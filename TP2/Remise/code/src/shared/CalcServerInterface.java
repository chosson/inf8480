package shared;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface CalcServerInterface extends Remote {
	int execute (String username, String password, ArrayList<String> ops) throws RemoteException;
	int getMaxOp() throws RemoteException;
}
