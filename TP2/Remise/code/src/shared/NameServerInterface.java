package shared;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface NameServerInterface extends Remote {
	ArrayList<String> getAvailableServers() throws RemoteException;
	boolean isValidUserInfo(String username, String password) throws RemoteException;
	boolean registerCalcServer(String hostname) throws RemoteException;
}
