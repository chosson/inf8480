package shared;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by kepana on 18-02-14.
 */
public class Operation implements Serializable{
    public String cmd;
    public int arg;

    Operation(String cmd, int arg){
        this.cmd = cmd;
        this.arg = arg;
    }

    public static Operation fromString(String strOperation){
        String[] splitResult = strOperation.split(" +");
        return new Operation(splitResult[0], Integer.parseInt(splitResult[1]));
    }
}
