Utilisation du Calculateur.

Le logiciel utilise 3 services diff�rents:
Un logiciel de service de nom(name_server), un service de calculation (calc_server) et un service
r�partiteur(dispatcher).

Pour l'utilisation de la solution, il faut tout d'abord partir le name_server, ensuite les calc_servers 
et finalement le dispatcher.

*** Il est important de configurer le logiciel avant de l'utiliser

0. Configuration du logiciel:

	0.1: Configuration du logiciel:
		0.1.1: Inscrivez votre nom et votre mot de passe dans le fichier config/users.txt pour vous cr�er un compte sur le serveur de nom.
			   Sous  la forme : "username,password" **Pas d�espace avant et apr�s  �,�
	
		0.1.2: Choissiez quel ordinateur sera responsable du service de nom **Ne doit pas �tre l'ordinateur courant**
			   et notez son nom dans le fichier config/name_server.txt au lieu de celui qui est pr�sent.

	0.2: Dans le r�pertoire principal du projet, compilez le projet avec la commande ant.


1. Utilisation du service de nom (name_server):

	1.1: Ouvrez un terminal, se connecter en ssh � l'ordinateur choisi � l'�tape 0.1.1

	1.3: Naviguez dans le dossier bin du projet et entrer la commande rmiregistry 5000 &

	1.4: Revenir dans le dossier parent (cd..) et lancer la commande: ./name_server
	
	1.5: Le service de serveur de nom est maintenante pr�t � �tre utilis�.


2. D�marrage du service de calculation (calc_server)

	2.1: Choisissez un ou plusiurs ordinateurs et pour chacun ouvrez un terminal et connectez-vous en ssh � cet ordinateur.
	
	2.2: Naviguez dans le dossier bin du projet et entrer la commande rmiregistry 5000 &
	
	2.3: Ex�cuter la commande suivate pour lancer le serveur de calculation: 
		 ./calc_server <capacite_calcul> <error_rate> 
		 
		 <capacite_calcul> = int repr�sentant le nombre d'op�ration max garenti avant une possiblit� de refus de calculation
		 <error_rate> =	 double entre 0 et 1 repr�sentant le pourcentage d'envoi de r�ponse erron�e.
		 
		 
3. Utilisation du r�aprtiteur de calcul (dispatcher)

	3.1: Sur l'ordinateur courrant, naviguez dans le dossier bin du projet et entrez la commande rmiregistry 5000 &
	
	3.2: Revenir dans le dossier parent et executez la ligne de commande suivante pour d�marrer une t�che de calculation:
			./dispatcher <OperationsFile> <username> <password> <IsSafe>
			<OperationsFile> = chemin du fichier dans lequel se trouve les opr�ations � ex�cuter
			<username> = nom d'utilisateur
			<password> = mot de passe de l'utilisateur
			<IsSafe>: "true" si le dispatcher suppose que les r�sultats des serveurs de calculation sont toujours bon. 
					  "false" si le dispatcher doit avoir au moins la m�me r�ponse de deux serverur de calculation diff�rents.