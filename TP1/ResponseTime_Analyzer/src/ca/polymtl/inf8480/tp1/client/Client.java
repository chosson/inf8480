package ca.polymtl.inf8480.tp1.client;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import ca.polymtl.inf8480.tp1.shared.ServerInterface;

public class Client {
	private static int x = 0;
	private static byte[] array;
	private static long normalResult;
	private static long localRMIResult;
	private static long distantRMIResult;

	public static void main(String[] args) {
		String distantHostname = null;
		if (args.length > 0) {
			distantHostname = args[0];
			if (args.length > 1){
				x = Integer.parseInt(args[1]);
			    array = new byte[(int)(java.lang.Math.pow(10,x))];
			}
		}

		Client client = new Client(distantHostname);
		client.run();
	}

	FakeServer localServer = null;  // Pour tester la latence d'un appel de
									// fonction normal.
	private ServerInterface localServerStub = null;
	private ServerInterface distantServerStub = null;

	public Client(String distantServerHostname) {
		super();

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		localServer = new FakeServer();
		localServerStub = loadServerStub("127.0.0.1");

		if (distantServerHostname != null) {
			distantServerStub = loadServerStub(distantServerHostname);
		}
	}

	private void run() {
		appelNormal();

		if (localServerStub != null) {
			appelRMILocal();
		}

		if (distantServerStub != null) {
			appelRMIDistant();
		}

        // For python RunTests.py script. Used to easily plot the logarithmic graph
		  System.out.println(normalResult + "," + localRMIResult + "," + distantRMIResult);

		//System.out.println("Temps écoulé appel normal: " + (normalResult) + " ns");
		//System.out.println("Temps écoulé appel RMIlocal: " + (localRMIResult) + " ns");
		//System.out.println("Temps écoulé appel RMIdistant: " + (distantRMIResult) + " ns");
	}

	private ServerInterface loadServerStub(String hostname) {
		ServerInterface stub = null;

		try {
			Registry registry = LocateRegistry.getRegistry(hostname);
			stub = (ServerInterface) registry.lookup("server");
		} catch (NotBoundException e) {
			System.out.println("Erreur: Le nom '" + e.getMessage()
					+ "' n'est pas défini dans le registre.");
		} catch (AccessException e) {
			System.out.println("Erreur: " + e.getMessage());
		} catch (RemoteException e) {
			System.out.println("Erreur: " + e.getMessage());
		}

		return stub;
	}

	private void appelNormal() {
		long start = System.nanoTime();
		int result = localServer.execute(array);
		long end = System.nanoTime();
        normalResult = (end - start);
	}

	private void appelRMILocal() {
		try {
			long start = System.nanoTime();
			int result = localServerStub.execute(array);
			long end = System.nanoTime();
            localRMIResult = (end - start);

		} catch (RemoteException e) {
			System.out.println("Erreur: " + e.getMessage());
		}
	}

	private void appelRMIDistant() {
		try {
			long start = System.nanoTime();
			int result = distantServerStub.execute(array);
			long end = System.nanoTime();
			distantRMIResult = (end-start);
		} catch (RemoteException e) {
			System.out.println("Erreur: " + e.getMessage());
		}
	}
}
