import subprocess
import sys
from matplotlib import pyplot

ip = '132.207.12.77'
xp = [1, 2, 3, 4, 5, 6, 7]
ny = []
ly = []
dy = []

if len(sys.argv) > 1:
	maximum = int(sys.argv[1])
else:
	maximum = 5

for i in range(1, 8):
    x = str(i)
    averageNormal = 0
    averageLocal = 0
    averageDist = 0

    for j in range(1, maximum + 1):
        output = subprocess.check_output(['./client', ip, x])
        output = output.split(",")
        averageNormal += int(output[0])
        averageLocal += int(output[1])
        averageDist += int(output[2])
    averageNormal /= (maximum)
    ny.append(averageNormal)
    averageLocal /= (maximum)
    ly.append(averageLocal)
    averageDist /= (maximum)
    dy.append(averageDist)
    print ('x={}, Normal {}, Local {}, Distant {}'.format(x, averageNormal, averageLocal, averageDist))

pyplot.plot(xp, ny, label="Appel Normal")
pyplot.plot(xp, ly, label="Appel RMI Local")
pyplot.plot(xp, dy, label="Appel RMI Distant")
pyplot.xlabel('x')
pyplot.ylabel('time')
pyplot.title('Execution time')
pyplot.legend()
pyplot.yscale('log')

pyplot.show()
