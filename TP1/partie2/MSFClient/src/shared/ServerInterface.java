package shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {

	int CreateClientID() throws RemoteException;

	boolean create(String name) throws  RemoteException;

	String list() throws  RemoteException;

	String[] syncLocalDirectory() throws RemoteException;

	byte[] get(String name, byte[] checksumm) throws RemoteException;

	byte[] lock (String name, int clientid, byte[] checksum) throws Exception;

	boolean push(String name, byte[] contents, int clientid) throws RemoteException;
}
