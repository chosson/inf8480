package shared;

/**
 * Created by kepana on 18-02-06.
 */
public class FileLockedException extends Exception {

    // Parameterless Constructor
    public FileLockedException() {}

    // Constructor that accepts a message
    public FileLockedException(String message)
    {
        super(message);
    }

}
