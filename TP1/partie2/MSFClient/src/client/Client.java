package client;

import java.io.*;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.regex.Pattern;

import client.commands.*;
import shared.ServerInterface;

public class Client {

    // Environment directory
    // Important, if changed must bu changed in BaseCommand also
    private static String DIRPATH = "FileSystem";

    // indicates if the server is remote
    private static boolean isDistant = false;

    private ServerInterface serverStub = null;

    // Regex to validate server ip address
    private static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");


    public static void main(String[] args) {

        // Create the environment directories
        File dir = new File(DIRPATH);
        dir.mkdirs();

        String distantHostname = null;
        if (args.length > 0) {
            distantHostname = args[0];
        } else {
            System.out.println("Not Enough Arguments Exception.");
            return;
        }

        Client client = new Client(distantHostname);
        client.run(args);

    }



    public Client(String distantServerHostname) {

        super();
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        if (distantServerHostname != null && validateIP(distantServerHostname)) {
            isDistant = true;
            serverStub = loadServerStub(distantServerHostname);
        }

        if (serverStub == null){
            isDistant = false;
            serverStub = loadServerStub("127.0.0.1");
        }
    }

    private void run(String[] args) {

        if (serverStub != null) {
            appelRMI(args);
        } else {
            System.out.println("Unable to start client/server connection");
        }

    }

    private ServerInterface loadServerStub(String hostname) {
        ServerInterface stub = null;
        try {
            Registry registry = LocateRegistry.getRegistry(hostname);
            stub = (ServerInterface) registry.lookup("server");
        } catch (NotBoundException e) {
            System.out.println("Error: The name '" + e.getMessage() + "' Is not defined in the registry.\n");
        } catch (AccessException e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        } catch (RemoteException e) {
            System.out.println("Error : " + e.getMessage() + "\n");
        }

        return stub;
    }

    public static boolean validateIP(final String ip) {
        return PATTERN.matcher(ip).matches();
    }

    private void appelRMI(String[] args){
        try {
            String filename = "";
            String cmd = "";
            int offset = isDistant? 1 : 0;
            if (args.length > 0 + offset){
                cmd = args[0 + offset];
                if (args.length > 1 + offset){
                    filename = args[1 + offset];
                }
            }

            BaseCommand command;
            switch (cmd) {
                case "CreateClientID":
                    command = new CreateClientIDCommand(serverStub);
                    break;

                case "create":
                    command = new CreateFileCommand(serverStub, filename);
                    break;

                case "list":
                    command = new ListCommand(serverStub);
                    break;

                case "syncLocalDirectory":
                    command = new SyncLocalCommand(serverStub, DIRPATH);
                    break;

                case "get":
                    command = new GetFileCommand(serverStub, filename, DIRPATH);
                    break;

                case "lock":
                    command = new LockFileCommand(serverStub, filename, DIRPATH);
                    break;

                case "push":
                    command = new PushFileCommand(serverStub, filename, DIRPATH);
                    break;

                default:
                    command = new BaseCommand(serverStub);
                    break;
            }
            command.Execute();

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
