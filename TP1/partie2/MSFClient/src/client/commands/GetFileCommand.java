package client.commands;

import shared.ServerInterface;

/**
 * Created by kepana on 18-02-10.
 */
public class GetFileCommand extends BaseCommand{

    private String filename;
    private String dirpath;

    public GetFileCommand(ServerInterface stub, String filename, String dirpath){
        super(stub);
        this.filename = filename;
        this.dirpath = dirpath;
    }

    public void Execute() throws Exception{
        if (getCID() == 0){
            System.out.println("Create a ClientID first.");
            return;
        }
        if (filename == "" ){
            System.out.println("To get a file from the remote, you must enter a filename.");
        }
        getFileFromRemote(filename, dirpath);

    }

}
