package client.commands;

import shared.MD5Checksum;
import shared.ServerInterface;

/**
 * Created by kepana on 18-02-10.
 */
public class LockFileCommand extends BaseCommand{

    private String filename;
    private String dirpath;

    public LockFileCommand(ServerInterface stub, String filename, String dirpath){
        super(stub);
        this.filename = filename;
        this.dirpath = dirpath;
    }



    public void Execute() throws Exception{
        if (getCID() == 0){
            System.out.println("Create a ClientID first.");
            return;
        }
        if (filename == "" ){
            System.out.println("To lock a file, you must enter a filename.");
        }
        String filePath = dirpath + "/" + filename;

        try {
            byte[] data = serverStub.lock(filename, getCID(), MD5Checksum.getMD5Checksum(filePath));
            if (data != null)
            {
                writeDataToFile(data, filePath);
            }
            System.out.println(filename + " successfully locked.");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
