package client.commands;

import shared.ServerInterface;

/**
 * Created by kepana on 18-02-10.
 */
public class CreateFileCommand extends BaseCommand{

    private String filename;

    public CreateFileCommand(ServerInterface stub, String filename){
        super(stub);
        this.filename = filename;
    }



    public void Execute() throws Exception{
        if (getCID() == 0){
            System.out.println("Create a ClientID first.");
            return;
        }
        if (filename == "" ){
            System.out.println("To create a file on the remote, you must enter a filename.");
        }
        if(serverStub.create(filename)){
            System.out.println(filename + " sucessfully created.");
        } else {
            System.out.println(filename+ " already exists on remote server.");
        }

    }

}
