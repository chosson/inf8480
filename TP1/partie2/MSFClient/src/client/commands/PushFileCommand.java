package client.commands;

import shared.ServerInterface;
import java.io.IOException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by kepana on 18-02-10.
 */
public class PushFileCommand extends BaseCommand{

    private String filename;
    private String dirpath;

    public PushFileCommand(ServerInterface stub, String filename, String dirpath){
        super(stub);
        this.filename = filename;
        this.dirpath = dirpath;
    }

    public void Execute() throws Exception{

        if (getCID() == 0){
            System.out.println("Create a ClientID first.");
            return;
        }
        if (filename == "" ){
            System.out.println("To push modifications of a file on the remote, you must enter a filename.");
        }
        byte [] contents = null;
        try {
            File file = new File(dirpath + "/" + filename);
            if (!file.exists()){
                System.out.println(filename + " doesn't exist locally.");
                return;
            }
            Path path = Paths.get(dirpath + "/" + filename);
            contents = Files.readAllBytes(path);
        } catch (IOException e) {
            System.out.print(e);
            return;
        }

        boolean pushed = serverStub.push(filename, contents, getCID());
        if (pushed){
            System.out.println("File modifications successfully sent to server.");
        } else {
            System.out.println("Lock the file before pushing changes.");
        }


    }

}
