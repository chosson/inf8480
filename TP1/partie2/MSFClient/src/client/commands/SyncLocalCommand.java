package client.commands;

import shared.ServerInterface;

/**
 * Created by kepana on 18-02-10.
 */
public class SyncLocalCommand extends BaseCommand{

    private String dirpath;

    public SyncLocalCommand(ServerInterface stub, String dirpath){

        super(stub);
        this.dirpath = dirpath;
    }

    public void Execute() throws Exception {
        String[] filesOnRemote = serverStub.syncLocalDirectory();
        for (String it : filesOnRemote) {
            getFileFromRemote(it, dirpath);
        }
    }
}
