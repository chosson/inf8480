package client.commands;

import shared.ServerInterface;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by kepana on 18-02-10.
 */
public class CreateClientIDCommand extends BaseCommand{

    public CreateClientIDCommand(ServerInterface stub){
        super(stub);
    }

    public void Execute() throws Exception{
        String filename = "clientID";
        File file = new File(filename);
        if(!file.exists()) {
            int id = serverStub.CreateClientID();
            Writer wr = new FileWriter(filename);
            try {
                file.createNewFile();
                wr.write(Integer.toString(id));
            } catch (IOException e) {
                wr.close();
                System.out.print("Could not create clientId file. Reason" + e);
            } finally {
                wr.close();
            }
        } else{
            System.out.print("You already have an ID: " + getCID());
        }
    }

}
