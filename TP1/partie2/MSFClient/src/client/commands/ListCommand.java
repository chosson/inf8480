package client.commands;

import shared.ServerInterface;

/**
 * Created by kepana on 18-02-10.
 */
public class ListCommand extends BaseCommand {

    public ListCommand(ServerInterface stub){
        super(stub);
    }

    public void Execute() throws Exception{
        if (getCID() == 0){
            System.out.println("Create a ClientID first.");
            return;
        }
        System.out.println(serverStub.list());
    }
}
