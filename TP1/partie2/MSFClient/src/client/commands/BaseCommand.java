package client.commands;

import shared.MD5Checksum;
import shared.ServerInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 * Created by kepana on 18-02-10.
 */
public class BaseCommand {

    protected ServerInterface serverStub;

    public void Execute() throws Exception{
        System.out.println("Unknown Command");
    }

    public BaseCommand(ServerInterface stub){
        this.serverStub = stub;
    }

    protected int getCID(){
        int clientId = 0;
        try {
            Scanner scanner = new Scanner(new File("clientID"));
            if (scanner.hasNextInt()) {
                clientId = scanner.nextInt();
            }
            scanner.close();
        } catch (FileNotFoundException e){

        }
        return clientId;
    }

    protected void getFileFromRemote(String filename, String dirpath){
        String filePath = dirpath + "/" + filename;
        File file = new File(filePath);
        byte[] checksum = null;

        if(file.exists()){
            checksum = MD5Checksum.getMD5Checksum(filePath);
        }
        try{
            byte[] data = serverStub.get(filename,checksum);

            if(data != null) {
                writeDataToFile(data, filePath);
                System.out.println(filename + " successfully synchronised");
            } else {
                System.out.println(filename + " already synced with remote or file doesn't exist on remote");
            }
        } catch(RemoteException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    protected void writeDataToFile(byte[] data, String filePath) {
        try {
            File file = new File(filePath);
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(data);
            fos.close();
        } catch (IOException e){
            System.out.println(e);
        }

    }

}
