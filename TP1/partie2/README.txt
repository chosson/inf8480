Utilisation du Micro Systeme de fichier.


Le logiciel est séparé en deux.

Un application Serveur(MSFServer) et un application Client(MSFClient).

1. Utilisation du Serveur:
	
	1.1: Ouvrez un terminal dans le dossier MSFServer

	1.2: Compilez le projet en entrant la commande ant

	1.3: Naviguez dans le dossier bin et entrer la commande rmiregistry &

	1.4: Revenir dans le dossier parent et lancer la commande:
		1.4.1: ./server si le serveur est utilisé sur la machine locale;
		1.4.2: ./serverd si le serveur est sur une machine distante
			p.s. il faudra modifier le script ./serverd pour entrer la bonne addresse ip
			     de la machine distante. 

	1.5: Le serveur est prêt à être utilisé.
	     ***Les fichiers créés se trouveront dans le dossier FileSystem dans le répertoire courant
	     ***Les informations sur le nombre de clients se trouvent dans le fichier clientCount (répertoire courrant)
	     ***Les informations sur les verrous se trouvent dans le dossier /FileSystem/.locks


2. Utilisation du Client

	2.1: Ouvrez un terminal dans le dossier MSFClient

	2.2: Compilez le projet en entrant la commande ant
	
	2.3: Le client peut être lancé de deux façons:
		2.3.1: ./client [commande] [args] ex.: ./client CreateClientID
		2.3.2: ./client [ip_distante] [commande] [args] ex.: ./client 132.207.12.77 create fichier1

	2.4: Voici la liste de commandes disponnibles:
		***Le fichiers sont créés dans le dossier FileSystem dans le répertoire courant.
		***L'information relative au clientId se trouve dans le fichier clientID dans le répertoire courant.

		- CreateClientID: Doit être la première commande lancée afin d'avoir un identifiant.

		- create [filename]: Crée un fichier vide portant le nom [filename] sur le serveur.

		- list: Retourne la liste de fichiers présents sur le serveur.	

		- syncLocalDirectory: Récupère tous les fichiers se trouvant sur le serveur et les mets
		  		      à jour sur le client.		

		- get [filename]: Récupère le fichier [filename] si il existe sur le serveur et met à jour son
			          son contenu sur le client

		- lock [filename]: Verrouille le fichier [filename] si il existe sur le serveur et si
				   il n'est pas déja verrouillé. De plus, le contenu du fichier est mis à
				   jour sur dans le répertoire local (ajouté si il n'existe pas localement).

		- push [filename]: Envoie les modification faites sur le fichier [filename] au serveur si
				   le fichier était verrouillé par le client actuel. Après cet opération,
				   le verrou du fichier est retiré. Si le fichier n'existait pas sur le serveur,
				   il y est ajouté.
		
	
	

