package server;

import java.io.*;
import java.io.IOException;
import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.Arrays;


import shared.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;


public class Server implements ServerInterface {

    Server(){ super();}

    // Environment directories
    private static String FILESYSDIRPATH = "FileSystem";
    private static String FILESLOCKPATH = FILESYSDIRPATH + "/.locks";

    private int numberOfClients = getClientCount();

    // Map containing the locks on file information
    private ConcurrentHashMap<String, Integer> fileLocks = null;

    // Lock used to safely increment the user count.
    private ReentrantLock incrementLock = new ReentrantLock();


	public static void main (String[] args) {

		// Create the remoteFile system directory.
        File dir = new File(FILESYSDIRPATH);
        dir.mkdirs();

        // Create the .locks folder for handling locks persistence.
        dir = new File(FILESLOCKPATH);
        dir.mkdirs();

		Server server = new Server();
		server.run();
	}

	private void run()
	{

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		try {
			ServerInterface stub = (ServerInterface) UnicastRemoteObject.exportObject(this, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("server", stub);
			System.out.println("Server ready.");

			// Initiate the locks map.
            initFileLocks();

		} catch (ConnectException e) {
			System.err.println("Cannot connect to RMI registry. Has rmiregistry been started?\n");
			System.err.println("Error: " + e.getMessage());
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	}

	/**
	 *
	 * Creates the client ID, increments it, and returns it to the client.
	 *
	 * */
	@Override
	public int CreateClientID() throws RemoteException {
	    incrementLock.lock();
	    try {
	        ++numberOfClients;
            updateClientCount(numberOfClients);
        }finally {
	        incrementLock.unlock();
        }
        return numberOfClients;
	}

	/**
	 *
	 * Creates a file in the ServerFileSystemDirectory depending on the filename
	 *
	 * */
	@Override
	public boolean create(String name) throws  RemoteException {
        try{
            File f = new File(FILESYSDIRPATH + "/"+ name);
            if (!f.exists()) {
				f.createNewFile();
				fileLocks.put(name, 0);
				return true;
			}
		} catch (IOException e){
        	System.out.println(e);
		}
		return false;
	}

	/**
	 *
	 * Returns a list of all the available files in the software's FileSystem and
	 * also information on who owns the file (locks).
	 *
	 * */
	@Override
	public String list() throws RemoteException{
		String[] ls = getList(FILESYSDIRPATH);

		Arrays.sort(ls, String.CASE_INSENSITIVE_ORDER);

		String ret = "";
		for (String item: ls) {
			Integer uid = fileLocks.get(item);
			ret += ("- " + item);
			if (uid == null ||uid == 0){
				ret += "   unlocked";
			} else {
				ret += ("   locked by client " + uid);
			}
			ret += "\n";
		}
		if (ret != null && ret.length() > 0) {
			ret = ret.substring(0, ret.length() - 1);
		}
		return ret;
	}

	/**
	 *
	 * Returns a list of current available files to sync with
	 *
	 * */
	@Override
	public String[] syncLocalDirectory() throws RemoteException {
		return getList(FILESYSDIRPATH);
	}


	/**
	 *
	 * Returns the current file content to the client.
	 *
	 * */
	@Override
	public byte[] get(String name, byte[] checksum) throws RemoteException {
	    byte[] data = null;
        byte[] localChecksum = MD5Checksum.getMD5Checksum(FILESYSDIRPATH + "/" + name);

        // If the client doesn't have the file, send it to him.
	    if (checksum == null ||  !Arrays.equals(checksum, localChecksum)) {
            try {
                File file = new File(FILESYSDIRPATH + "/" + name);
                if(file.exists()) {
                    Path path = Paths.get(FILESYSDIRPATH + "/" + name);
                    data = Files.readAllBytes(path);
                }
            } catch (IOException e) {
                System.out.print(e);
            }
        }
        return data;
    }

	/**
	 *
	 * Locks the given file if it exists.
	 * It also adds reference file with the same filename in the .locks folder
	 * containing the user's id to determine if the file is locked. In the map,
	 * 0 represents an unlocked file and any other value is locked by that client id.
	 *
	 * */
	@Override
    public byte[] lock(String name, int clientid, byte[] checksum) throws Exception {

	    Integer ownerId = fileLocks.get(name);
	    if (ownerId == null){
	        throw new FileLockedException(name + " doesn't exist.");
        }
        if (fileLocks.get(name) == 0) {
        	//Add the lock to the list and add a file to the .locks folder
        	fileLocks.put(name, clientid);
			File file = new File(FILESLOCKPATH + "/" + name);
        	Writer wr = new FileWriter(FILESLOCKPATH + "/" + name);
			try {
				file.createNewFile();
				wr.write(Integer.toString(clientid));
			} catch (IOException e) {
				wr.close();
			}finally {
				wr.close();
			}

        } else {
            throw new FileLockedException(name + " already locked by Client " + fileLocks.get(name));
        }

        byte[] localChecksum = MD5Checksum.getMD5Checksum(FILESYSDIRPATH + "/" + name);
        if(!Arrays.equals(localChecksum,checksum)){
            return get(name,null);
        }

        return null;
    }

    /**
	 *
	 * Saves the file received from the client and removes the lock he had on the file.
	 * If the file doesn't exist, create it.
	 *
	 * */
	@Override
	public boolean push(String filename, byte[] contents, int clientid) throws  RemoteException {

		Integer fileOwnerID = (fileLocks.get(filename));
		if(fileOwnerID != null && fileOwnerID != clientid){
			return false;
		}
        overwriteDataToFile(contents, FILESYSDIRPATH + "/" + filename);

		//Update file lok status in the list.
		fileLocks.replace(filename, 0);

		//Remove file from the .locks folder
        File d = new File(FILESLOCKPATH + "/" + filename);
        if (d.exists()){
            d.delete();
        }
		return true;
	}


	/*
	*
	* PRIVATE METHODS
	*
	* */

	private int getClientCount(){

        int clientCount = 0;
        try {
            Scanner scanner = new Scanner(new File("clientCount"));
            if (scanner.hasNextInt()) {
                clientCount = scanner.nextInt();
            }
            scanner.close();
        } catch (FileNotFoundException e){
            updateClientCount(0);
        }
        return clientCount;
	}


	private void overwriteDataToFile(byte[] data, String filePath) {
        try {
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(data);
            fos.close();
        } catch (IOException e){
        	System.out.println(e);
		}
    }

    /**
	 *
	 * Initializes the fileLocks map, it gets is full directory from
	 * the main FileSystem directory and it gets the locks info from
	 * the .locks directory.
	 *
	 * */
	private void initFileLocks(){

		fileLocks = new ConcurrentHashMap<>();

		// Load FileSystem directory filenames
		String[] items = getList(FILESYSDIRPATH);
		for (String itt : items) {
			fileLocks.put(itt,0);
		}

		// Load locks from .locksFolder
		items = getList(FILESLOCKPATH);
		for (String itt: items){
			try {
				Scanner scanner = new Scanner(new File(FILESLOCKPATH + "/" + itt));
				if (scanner.hasNextInt()) {
					fileLocks.replace(itt, scanner.nextInt());
				}
				scanner.close();
			} catch (FileNotFoundException e){}
		}
	}

	private String[] getList(String path){
        File folder = new File(path + "/");
        File[] listOfFiles = folder.listFiles();
        LinkedList<String> Files = new LinkedList<>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                Files.add(listOfFiles[i].getName());
            }
        }
        String[] ret = new String[Files.size()];
        Files.toArray(ret);
        return ret;
    }

    private void updateClientCount(int newcount){
        try {
			File file = new File("clientCount");
            if (!file.exists()){
				file.createNewFile();
			}
            Writer wr = new FileWriter("clientCount");
            wr.write(Integer.toString(newcount));
            wr.close();
        } catch (IOException e) {
            System.out.print("Could not create clientCount file. Reason" + e);
        }
    }
}